namespace Vidly.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddTorental1 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Rentals", "DateReturned", c => c.DateTime());
            AlterColumn("dbo.Rentals", "DateRanted", c => c.DateTime(nullable: false));
            DropColumn("dbo.Rentals", "DateTime");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Rentals", "DateTime", c => c.DateTime(nullable: false));
            AlterColumn("dbo.Rentals", "DateRanted", c => c.DateTime());
            DropColumn("dbo.Rentals", "DateReturned");
        }
    }
}
