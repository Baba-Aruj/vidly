﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Results;
using Vidly.Dto;
using Vidly.Models;

namespace Vidly.Controllers.Api
{
    public class NewRentalsController : ApiController
    {
        private ApplicationDbContext _context;
        public NewRentalsController()
        {
            _context = new ApplicationDbContext();
        }
        protected override void Dispose(bool disposing)
        {
            _context.Dispose();
        }

        [HttpPost]
        public IHttpActionResult CreateNewRentals(RentalDto rentalDto)
        {
            
            var customer = _context.Customers.Single(c => c.Id == rentalDto.CUstomerId);
            
            var movies = _context.Movies.Where(
                m =>  rentalDto.MovieIds.Contains(m.Id)).ToList();
        

            foreach (var movie in movies)
            {
                if (movie.NumberAvailable== 0)
                    return BadRequest(" Movie is not availlabe");
                movie.NumberAvailable--;
                var rental = new Rental
                {
                    customer = customer,
                    Movie = movie,
                    DateRanted = DateTime.Now
                };
                _context.Rentals.Add(rental);

            }
            _context.SaveChanges();
            return Ok();

        }
    }
}
