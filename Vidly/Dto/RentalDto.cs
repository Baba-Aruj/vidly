﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Vidly.Models;

namespace Vidly.Dto
{
    public class RentalDto
    {
        public int CUstomerId { get; set; }
        public List<int> MovieIds { get; set; }
    }
}